from flask import *
from codeAPI import myoswrap
from codeAPI.customExceptions import *

app = Flask('TP2 API')


def getUserList():
	users = []
	for line in myoswrap.readFileByLine('/etc/passwd'):
		users.append(line.split(':')[0])
	return users

#TODO fill this up properly
INITIAL_USERS = ['root']

def isInitialUser(username):
	if username in INITIAL_USERS:
		return True
	return False



#Done classical method to back related route (unit test this)
def hello():
	return 'Welcome to user hot program!'

#Done route method (test with deployment tests)
@app.route('/')
def route_hello(): #pragma: no cover
	return jsonify({'code':'2000', 'msg':hello()})



#classical method to back related route
def getUsers():
	#TODO
	return None

#route method
@app.route('/getusers')
def route_getUsers(): #pragma: no cover
	#TODO
	return "Work in progress"




#done classical method to back related route
def delUser(username):
	if username in getUserList():
		if isInitialUser(username):
			raise InitialUserException('cannot delete an initial user.')

		else:
			myoswrap.runCommandToRemoveUser(username)
			return 'deleted ' + username
	raise NonExistingUserException('The specified user (' + username + ') does not exist, cannot perform delete operation.')

#done route method
@app.route('/deluser')
def route_delUser(): #pragma: no cover
	if 'username' in request.args:
		username = request.args['username']
		try:
			rep = delUser(username)
			return jsonify({'code':'2000', 'msg':rep})
		except InitialUserException as iue:
			return make_response(jsonify({'code':'1003', 'msg':str(iue)}), 400)
		except NonExistingUserException as neue:
			return make_response(jsonify({'code':'1002', 'msg':str(neue)}), 400)
	else:
		return make_response(jsonify({'code':'1000', 'msg':'param username is mandatory for deletion.'}), 400)




#classical method to back related route
def resetUsers():
	#TODO
	return None

#route method 
@app.route('/resetusers')
def route_resetUsers(): #pragma: no cover
	#TODO
	return "Work in progress"




#classical method to back related route
def addUser(username):
	#TODO
	return None

#route method 
@app.route('/adduser')
def route_addUser(): #pragma: no cover
	#TODO
	return "Work in progress"





#classical method to back related route
def getLogs():
	#TODO
	return None

#route method 
@app.route('/getlog')
def route_getLogs(): #pragma: no cover
	#TODO
	return "Work in progress"




if __name__ == "__main__": #pragma: no cover
	app.run(debug=True, host='0.0.0.0', port=5555)

