import unittest
import json
import unittest.mock

from codeAPI.customExceptions import *
from codeAPI import userAPI


class BasicTests(unittest.TestCase):

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_hello(self, mock_oswrap):
		actual = userAPI.hello()
		self.assertIn('Welcome', actual)
		#check the three possible mock calls
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_not_called()


	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_delUser(self, mock_oswrap):
		#mocking a return value for one method (so the system appears to have 1 users overall)
		mock_oswrap.readFileByLine.return_value = ['Roy:x:1000:1000:Roy:/home/Roy:/bin/bash']
		obj = userAPI.delUser('Roy')
		#check the three possible mock calls
		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToRemoveUser.assert_called_with('Roy')
		mock_oswrap.runCommandToAddUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_delUserNonExisting(self, mock_oswrap):
		with self.assertRaises(NonExistingUserException) as context:
			userAPI.delUser('Brodeur')
			#check the three possible mock calls
			mock_oswrap.runCommandToAddUser.assert_not_called()
			mock_oswrap.runCommandToRemoveUser.assert_not_called()
			mock_oswrap.readFileByLine.assert_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_delInitialUser(self, mock_oswrap):
		with self.assertRaises(InitialUserException) as context:
			#mocking a return value for one method (so the system appears to have 1 users overall)
			mock_oswrap.readFileByLine.return_value = ['root:x:0:0:root:/home/root:/bin/bash']
			userAPI.delUser('root')
			#check the three possible mock calls
			mock_oswrap.runCommandToAddUser.assert_not_called()
			mock_oswrap.runCommandToRemoveUser.assert_not_called()
			mock_oswrap.readFileByLine.assert_called()

if __name__ == '__main__':
	unittest.main()

